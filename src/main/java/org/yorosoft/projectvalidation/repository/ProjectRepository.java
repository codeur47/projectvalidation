package org.yorosoft.projectvalidation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.yorosoft.projectvalidation.model.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
}
