package org.yorosoft.projectvalidation.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Project extends AbstractAuditingEntity{

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long projectId;

    private String name;

    private String description;

}
