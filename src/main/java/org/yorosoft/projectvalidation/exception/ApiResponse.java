package org.yorosoft.projectvalidation.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResponse {

    // other values
    private ApiError error;

    public ApiResponse(ApiError apiError) {
        this.error = apiError;
    }
}
