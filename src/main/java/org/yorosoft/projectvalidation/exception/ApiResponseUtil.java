package org.yorosoft.projectvalidation.exception;

public class ApiResponseUtil {
    public static ApiResponse error(int value, String message) {
        return new ApiResponse(new ApiError(value, message));
    }

    public static ApiResponse error(ApiError apiError) {
        return new ApiResponse(apiError);
    }
}