package org.yorosoft.projectvalidation.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
class ApiError {
    private Integer statusCode;
    private String message;
    private List<ValidationError> errors;

    public ApiError(Integer statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
    }

    public void addValidationError(String field, Object value, String message) {
        if (errors == null)
            errors = new ArrayList<>();

        ValidationError validationError = new ValidationError(field, value, message);
        errors.add(validationError);
    }

}