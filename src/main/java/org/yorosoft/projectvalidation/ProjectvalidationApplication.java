package org.yorosoft.projectvalidation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectvalidationApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectvalidationApplication.class, args);
	}

}
