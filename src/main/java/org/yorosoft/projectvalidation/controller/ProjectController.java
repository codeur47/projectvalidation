package org.yorosoft.projectvalidation.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.yorosoft.projectvalidation.config.ProjectControllerAPIPaths;
import org.yorosoft.projectvalidation.mapstruct.dto.ProjectRequest;
import org.yorosoft.projectvalidation.mapstruct.mapper.ProjectMapper;
import org.yorosoft.projectvalidation.service.ProjectService;

import javax.validation.Valid;

@RestController
@RequestMapping(ProjectControllerAPIPaths.BASE_URL)
@AllArgsConstructor
public class ProjectController {

    private final ProjectService projectService;
    private final ProjectMapper projectMapper;

    @PostMapping
    public ResponseEntity<?> create(@Valid @RequestBody ProjectRequest projectRequest){
        return new ResponseEntity<>(projectService.create(projectMapper.mapProjectRequestToEntity(projectRequest)), HttpStatus.CREATED);
    }
}
