package org.yorosoft.projectvalidation.config;

public class ProjectControllerAPIPaths {
    public static final String BASE_URL = "/api/projectValidation";

    private ProjectControllerAPIPaths() {}
}
