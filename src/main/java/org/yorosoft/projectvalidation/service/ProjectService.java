package org.yorosoft.projectvalidation.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.yorosoft.projectvalidation.model.Project;
import org.yorosoft.projectvalidation.repository.ProjectRepository;

@Service
@AllArgsConstructor
public class ProjectService {

    private final ProjectRepository projectRepository;

    public Project create (Project project) {
        return projectRepository.save(project);
    }
}
