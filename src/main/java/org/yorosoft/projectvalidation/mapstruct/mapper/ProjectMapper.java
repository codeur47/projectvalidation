package org.yorosoft.projectvalidation.mapstruct.mapper;

import org.mapstruct.Mapper;
import org.yorosoft.projectvalidation.mapstruct.dto.ProjectRequest;
import org.yorosoft.projectvalidation.mapstruct.dto.ProjectResponse;
import org.yorosoft.projectvalidation.model.Project;

@Mapper(componentModel = "spring")
public interface ProjectMapper {

    Project mapProjectRequestToEntity(ProjectRequest projectRequest);

    Project mapProjectResponseToEntity(ProjectResponse projectResponse);

    ProjectResponse mapEntityToProjectResponse(Project project);

    ProjectRequest mapEntityToProjectRequest(Project project);
}
