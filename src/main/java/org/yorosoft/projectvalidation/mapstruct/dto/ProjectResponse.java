package org.yorosoft.projectvalidation.mapstruct.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.yorosoft.projectvalidation.model.AbstractAuditingEntity;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProjectResponse extends AbstractAuditingEntity {
    private Long projectId;

    private String name;

    private String description;
}
